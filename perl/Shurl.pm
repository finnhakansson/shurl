package Shurl;

use strict;
use Data::Dumper;
use Digest::SHA qw|sha1_base64|;
use HTML::Entities;
use Regexp::Common qw|URI|;
use Apache2::Const;
use Apache2::RequestRec ();
use Apache2::Request;
use Apache2::RequestIO ();
use Apache2::Response ();
use Store;
use constant SHURL_URL_TTL => 7 * 24 * 60 * 60; # One week.
use constant SHURL_URL_BASE => 'https://haqqx.com/s';
use constant HASH_LEN => 7;

my $debug = 0;


sub make_hash {
    my ($url) = @_;
    my $b64 = sha1_base64($url . $$ . time() . rand());
    $b64 =~ tr|+/|-_|;
    return substr($b64, 0, HASH_LEN);
}


sub valid_url {
    my ($url) = @_;
    my $url_copy = $url;
    $url_copy =~ s|^https|http|i;
    if ($url_copy =~ $RE{URI}{HTTP}) {
        return 'OK';
    }
    warn "Bad URL: \"$url\"\n" if $debug;
    return 0;
}


sub handler {
    my $r = shift;
    my $req = Apache2::Request->new($r, DISABLE_UPLOADS => 1);
    my $id = $req->param('id') || '';
    my $url = $req->param('url') || '';

    if ((!$id && !$url) || ($id && $url)) {
        # Nothing to do here. Display start page.
        start_page($r);
    } elsif (!$id) {
        if (valid_url($url)) {
            $id = make_hash($url);
            my $success = Store::store($id, $url, SHURL_URL_TTL);
            if ($success) {
                display_result($r, $id, $url);
            } else {
                warn "Failure storing in Memcached. " . Dumper($success);
            }
        } else {
            start_page($r);
        }
    } elsif (!$url) {
        $url = Store::retrieve($id);
        if (!$url) {
            bad_lookup($r, $id);
        } else {
            $r->headers_out->set(Location => $url);
            $r->status(Apache2::Const::REDIRECT);
        }
    }

    return Apache2::Const::OK;
}


sub start_page {
    my ($r) = @_;
    $r->content_type('text/html');
    $r->print("<html>\n<head><title>Shurl</title></head>\n<body>\n");
    $r->print("<h1>Shorten Your URL Here</h1>\n");
    $r->print("<form action=\"/s\" method=\"POST\">\n");
    $r->print("<input type=\"text\" name=\"url\" /><br />\n");
    $r->print("<button name=\"submit\" type=\"submit\" value=\"submit\">Submit</button>\n");
    $r->print("</form>\n");
    $r->print("</body>\n</html>\n");
}


sub bad_lookup {
    my ($r) = @_;
    $r->content_type('text/html');
    $r->print("<html>\n<head><title>Expired or Erroneous shortened URL</title></head>\n<body>\n");
    $r->print("<h1>Expired or Erroneous shortened URL</h1>\n");
    $r->print("Sorry.<br />\n");
    $r->print("</body>\n</html>\n");
}

sub display_result {
    my ($r, $id, $url) = @_;
    $r->content_type('text/html');
    $r->print("<html><head><title>Your Shortened URL</title></head><body><h1>Your Shortened URL</h1>\n");
    $r->print("<span>Your shortened URL:</span> <span><textarea readonly rows=\"1\" cols=\"70\" style=\"resize: none;\">" . SHURL_URL_BASE . '/' . $id . "</textarea></span><br />\n");
    $r->print("<span>Original URL:</span> <span>" . encode_entities($url) . "</span><br />\n");
    $r->print("</body></html>\n");
}


1;

