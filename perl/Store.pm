package Store;

use strict;
use Cache::Memcached::Fast;
use constant MEMCACHED_SERVERS => [ { address => 'localhost:11211' } ];

my $mem;

sub get_cache {
    if (!$mem) {
        $mem = new Cache::Memcached::Fast({servers => MEMCACHED_SERVERS});
    }
    return $mem;
}


sub store {
    my ($key, $val, $ttl) = @_;
    (get_cache())->set($key, $val, $ttl);
}


sub retrieve {
    my ($id) = @_;
    return (get_cache())->get($id);
}


1;

